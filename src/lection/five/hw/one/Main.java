package lection.five.hw.one;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        File folderIn = new File(".\\folderIN");
        File folderOut = new File(".\\folderOUT");
        String filesExt = "docx";

        try {
            long totalBytes = CopyService.copyFolder(folderIn, folderOut, filesExt);
            System.out.println("Copying " + totalBytes + " bytes");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}